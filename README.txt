
This is a wrapper module which will mimic the API interface of Job Queue to allow the modules which use job queue to use the backported Drupal Queues module http://drupal.org/project/drupal_queue and also allow the use of other modules like the backported beanstalkd.

The main reason I am writing this not because job queue is a bad module or doesn't do what it is said to do, but because the support for it is so large and it means that I can do some real world testing on my beanstalkd http://drupal.org/project/beanstalkd to see how well it is working.

Secondly my API site http://api.drupalecommerce.org/ which has a large number of contributed modules which it is tracking and the job queue module is just not keeping up even in running the cron every 10 minutes, and is days behind in indexing the current commits.